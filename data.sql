create database codeforfun;

use codeforfun;

CREATE TABLE `user` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `email` varchar(255) NOT NULL UNIQUE,
 `password` varchar(255) NOT NULL,
 `date_of_birth` datetime NOT NULL,
 `gender` bit NOT NULL,
 `full_name` varchar(50) NOT NULL,
 `user_name` varchar(30) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_role` (
 `user_id` int(11) NOT NULL,
 `role_id` int(11) NOT NULL,
 PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `posts` (
  `posts_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`posts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `posts` VALUES (1,'Feature/CV1.3 DHP-18236','postsId','<p>https://opensourceprojects.org/wp-content/uploads/2018/06/sql.png</p>\r\n\r\n<p><img alt=\"\" src=\"https://opensourceprojects.org/wp-content/uploads/2018/06/sql.png\" style=\"height:300px; width:574px\" /></p>\r\n',NULL),(2,'CV1.3_DHP-16227','1.3','<pre>\r\nth:action=&quot;@{/login}&quot;</pre>\r\n',NULL),(3,'DHP-19138 Check response when create patient setting in case input invalid paramValue','1.3','<p>DHP-19138 Check response when create patient setting in case input invalid paramValue</p>\r\n',NULL),(4,'DHP-19138 Check response when create patient setting in case input invalid paramValue','1.3','<p>DHP-19138 Check response when create patient setting in case input invalid paramValue</p>\r\n',NULL),(5,'CV1.3_DHP-18601 Enhance to delete/Update Terms&Conds','CV1.3_DHP-18601 Enhance to delete/Update Terms&Conds','<p>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;CondsCV1.3_DHP-18601 Enhance to delete/Update Terms&amp;CondsCV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</p>\r\n\r\n<p>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</p>\r\n\r\n<p><u>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</u></p>\r\n\r\n<p><em>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds&nbsp; &nbsp; &nbsp;CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</em></p>\r\n\r\n<p><strong>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"http://st.galaxypub.vn/staticFile/Subject/2017/03/28/484739/title_271727936.jpg\" style=\"height:720px; width:480px\" /></strong></p>\r\n',NULL),(6,'CV1.3_DHP-18601 Enhance to delete/Update Terms&Conds','1.3','<p>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;CondsCV1.3_DHP-18601 Enhance to delete/Update Terms&amp;CondsCV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</p>\r\n\r\n<p>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</p>\r\n\r\n<p><u>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</u></p>\r\n\r\n<p><em>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds&nbsp; &nbsp; &nbsp;CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</em></p>\r\n\r\n<p><strong>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"http://st.galaxypub.vn/staticFile/Subject/2017/03/28/484739/title_271727936.jpg\" style=\"height:720px; width:480px\" /></strong></p>\r\n',NULL),(7,'CV1.3_DHP-18601 Enhance to delete/Update Terms&Conds','1.3','<p>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;CondsCV1.3_DHP-18601 Enhance to delete/Update Terms&amp;CondsCV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</p>\r\n\r\n<p>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</p>\r\n\r\n<p><u>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</u></p>\r\n\r\n<p><em>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds&nbsp; &nbsp; &nbsp;CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</em></p>\r\n\r\n<p><strong>CV1.3_DHP-18601 Enhance to delete/Update Terms&amp;Conds</strong></p>\r\n\r\n<p><strong><img alt=\"\" src=\"http://st.galaxypub.vn/staticFile/Subject/2017/03/28/484739/title_271727936.jpg\" style=\"height:720px; width:480px\" /></strong></p>\r\n',NULL);
