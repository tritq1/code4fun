FROM hnpn11/codeforfun:v5

RUN cd ~ && rm -rf * && git clone -b nam_dev https://hnpn11@bitbucket.org/tritq1/code4fun && \
cd code4fun && \
sed -i 's/namhnp/NamHNP12345/g' ~/code4fun/src/main/resources/application.properties && \
sed -i 's/root/namhnp/g' ~/code4fun/src/main/resources/application.properties && \
mvn compile && mvn package && rm -rf /var/lib/tomcat8/webapps/* && \ 
cp ~/code4fun/target/*.war /var/lib/tomcat8/webapps/ROOT.war

CMD service mysql restart && service tomcat8 restart; /bin/bash

EXPOSE 8080