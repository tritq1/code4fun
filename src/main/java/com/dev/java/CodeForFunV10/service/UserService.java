package com.dev.java.CodeForFunV10.service;

import java.text.ParseException;

import com.dev.java.CodeForFunV10.domain.RegisterRequestDto;
import com.dev.java.CodeForFunV10.model.User;

public interface UserService {
	public boolean createUser(RegisterRequestDto registerRequestDto) throws ParseException;

	public boolean checkConfirmPassword(String password, String confirmPassword);

	boolean checkEmail(String email);
	
	boolean checkDateOfBirth(String dateOfBirth) throws ParseException;
}
