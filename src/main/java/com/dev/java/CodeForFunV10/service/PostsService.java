package com.dev.java.CodeForFunV10.service;

import com.dev.java.CodeForFunV10.domain.PostsRequestDto;
import com.dev.java.CodeForFunV10.domain.PostsResponseDto;

import java.util.List;

public interface PostsService {
    PostsResponseDto createNewPosts(PostsRequestDto requestDto);

    List<PostsResponseDto> getAllPosts();

    PostsResponseDto getOnePost(Long postId);
}
