package com.dev.java.CodeForFunV10.serviceImpl;

import com.dev.java.CodeForFunV10.domain.PostsRequestDto;
import com.dev.java.CodeForFunV10.domain.PostsResponseDto;
import com.dev.java.CodeForFunV10.model.Posts;
import com.dev.java.CodeForFunV10.repository.PostsRepository;
import com.dev.java.CodeForFunV10.service.PostsService;
import com.sun.org.apache.regexp.internal.recompile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostsService {
    @Autowired
    private PostsRepository postsRepository;

    @Override
    public PostsResponseDto createNewPosts(PostsRequestDto requestDto) {
        Posts posts = convertRequestDtoToPosts(requestDto);
        Posts postsResponse = postsRepository.save(posts);
        PostsResponseDto postsResponseDto = getPostsResponseDto(postsResponse);
        return postsResponseDto;
    }

    @Override
    public List<PostsResponseDto> getAllPosts() {
        List<Posts> postsList = postsRepository.findAll();
        List<PostsResponseDto> postsResponseDtos = new ArrayList<>();
        for(Posts posts : postsList) {
            PostsResponseDto postsResponseDto = getPostsResponseDto(posts);
            postsResponseDtos.add(postsResponseDto);
        }
        return postsResponseDtos;
    }

    @Override
    public PostsResponseDto getOnePost(Long postId) {
        return getPostsResponseDto(postsRepository.findPostsByPostsId(postId));
    }

    private PostsResponseDto getPostsResponseDto(Posts postsResponse) {
        PostsResponseDto postsResponseDto = new PostsResponseDto();
        postsResponseDto.setContent(postsResponse.getContent());
        postsResponseDto.setPostsId(postsResponse.getPostsId());
        postsResponseDto.setTag(postsResponse.getTag());
        postsResponseDto.setTitle(postsResponse.getTitle());
        postsResponseDto.setUserId(postsResponse.getUserId());
        return postsResponseDto;
    }

    private Posts convertRequestDtoToPosts(PostsRequestDto requestDto) {
        Posts posts = new Posts();
        posts.setUserId(requestDto.getUserId());
        posts.setContent(requestDto.getContent());
        posts.setTag(requestDto.getTag());
        posts.setTitle(requestDto.getTitle());
        return posts;
    }
}
