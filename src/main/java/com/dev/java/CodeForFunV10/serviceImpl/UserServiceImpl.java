package com.dev.java.CodeForFunV10.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.dev.java.CodeForFunV10.domain.RegisterRequestDto;
import com.dev.java.CodeForFunV10.model.User;
import com.dev.java.CodeForFunV10.repository.UserRepository;
import com.dev.java.CodeForFunV10.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public boolean createUser(RegisterRequestDto registerRequestDto) throws ParseException {
		User user = convertRegisterRequestDtoToUser(registerRequestDto);
		if(userRepository.save(user) == null){
			return false;
		}
		return true;
	}
	
	private User convertRegisterRequestDtoToUser(RegisterRequestDto registerRequestDto) throws ParseException{
		User user = new User();
		user.setEmail(registerRequestDto.getEmail());
		user.setPassword(passwordEncoder.encode(registerRequestDto.getPassword()));
		user.setFullName(registerRequestDto.getFullName());
		user.setUserName(registerRequestDto.getUserName());
		user.setDateOfBirth(new SimpleDateFormat("yyyy-MM-dd").parse(registerRequestDto.getDateOfBirth()));
		if(registerRequestDto.getGender().equals("Male")){
			user.setGender(true);
		} else {
			user.setGender(false);
		}
		return user;
	}

	@Override
	public boolean checkEmail(String email) {
		if(userRepository.findByEmail(email) != null){
			return false;
		}
		return true;
	}

	@Override
	public boolean checkConfirmPassword(String password, String confirmPassword) {
		if(password.equals(confirmPassword)){
			return true;
		}
		return false;
	}

	@Override
	public boolean checkDateOfBirth(String dateOfBirth) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format.parse(dateOfBirth);
		System.out.println((format.parse(format.format(new Date())).getTime() - date.getTime()) / (24 * 60 *60 *1000));
		if(((format.parse(format.format(new Date())).getTime() - date.getTime()) / (24 * 60 *60 *1000))> 10*365){
			return true;
		}
		return false;
	}
	
	
}
