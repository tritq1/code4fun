package com.dev.java.CodeForFunV10.domain;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterRequestDto {
	@NotEmpty
	private String email;
	@NotEmpty
	@Size(min = 8, max = 16)
	private String password;
	@NotEmpty
	@Size(min = 8, max = 16)
	private String confirmPassword;
	@NotEmpty
	@Size(max = 50)
	private String fullName;
	@NotEmpty
	@Size(max = 30)
	private String userName;
	@NotEmpty
	private String dateOfBirth;
	@NotEmpty
	private String gender;

	public RegisterRequestDto() {
		super();
	}

	public RegisterRequestDto(@NotEmpty String email, @NotEmpty @Size(min = 8, max = 16) String password,
			@NotEmpty @Size(min = 8, max = 16) String confirmPassword, @NotEmpty @Size(max = 50) String fullName,
			@NotEmpty @Size(max = 30) String userName, @NotEmpty String dateOfBirth, @NotEmpty String gender) {
		super();
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.fullName = fullName;
		this.userName = userName;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
