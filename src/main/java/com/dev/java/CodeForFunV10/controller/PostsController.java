package com.dev.java.CodeForFunV10.controller;
import com.dev.java.CodeForFunV10.domain.PostsRequestDto;
import com.dev.java.CodeForFunV10.domain.PostsResponseDto;
import com.dev.java.CodeForFunV10.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;

@Controller
public class PostsController {
    @Autowired
    private PostsService postsServiceImpl;

    @RequestMapping(value = "/create-posts", method = RequestMethod.POST)
    public String createPosts(@Valid PostsRequestDto requestDto, Model model){
        PostsResponseDto postsResponseDto =  postsServiceImpl.createNewPosts(requestDto);
        model.addAttribute("post", postsResponseDto);
        return "detail";
    }

    @RequestMapping(value = "/get-post/{postId}", method = RequestMethod.GET)
    public String getPosts(Model model, @PathVariable String postId){
        PostsResponseDto postsResponseDto = postsServiceImpl.getOnePost(Long.parseLong(postId));
        model.addAttribute("post", postsResponseDto);
        return "detail";
    }

}
