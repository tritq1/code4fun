package com.dev.java.CodeForFunV10.controller;

import com.dev.java.CodeForFunV10.domain.PostsResponseDto;
import com.dev.java.CodeForFunV10.domain.RegisterRequestDto;
import com.dev.java.CodeForFunV10.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private PostsService postsServiceImpl;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(Model model) {
        List<PostsResponseDto> postsResponseDtos = postsServiceImpl.getAllPosts();
        model.addAttribute("listPosts", postsResponseDtos);
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model, RegisterRequestDto registerRequestDto) {
//    	RegisterRequestDto registerRequestDto = new RegisterRequestDto();
//    	model.addAttribute("registerRequestDto", registerRequestDto);
        return "register";
    }

    @RequestMapping(value = "/add-new", method = RequestMethod.GET)
    public String newPost() {
        return "add-news";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }
}
