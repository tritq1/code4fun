package com.dev.java.CodeForFunV10.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.dev.java.CodeForFunV10.domain.RegisterRequestDto;
import com.dev.java.CodeForFunV10.service.UserService;

@Controller
public class UserController {
	@Autowired
	UserService userService;

	@PostMapping(value = "/register")
	public String register(@ModelAttribute("registerRequestDto") @Valid RegisterRequestDto registerRequestDto,BindingResult bindingResult, Model model) {
	System.out.println(bindingResult.hasErrors());
		if(bindingResult.hasErrors()){
			return "register";
		}
		if(!userService.checkEmail(registerRequestDto.getEmail())){
			model.addAttribute("message","Email is existed");
			return "register";
		}
		if(!userService.checkConfirmPassword(registerRequestDto.getPassword(), registerRequestDto.getConfirmPassword())){
			model.addAttribute("message", "Confirm password must match password");
			return "register";
		}
		try {
			if(!userService.checkDateOfBirth(registerRequestDto.getDateOfBirth())){
				model.addAttribute("message", "Age must be 10+");
				return "register";
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		try {
			if (userService.createUser(registerRequestDto)) {
				model.addAttribute("message", "Register Successfully");
				return "login";
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		model.addAttribute("message", "Register Failed");
		return "register";
	}
}
