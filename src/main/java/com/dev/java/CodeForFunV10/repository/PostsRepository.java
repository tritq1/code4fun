package com.dev.java.CodeForFunV10.repository;

import com.dev.java.CodeForFunV10.model.Posts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PostsRepository extends JpaRepository<Posts, Long> {

    Posts findPostsByPostsId(Long postId);
}
